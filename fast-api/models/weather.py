from tortoise import fields
from tortoise.models import Model


class WeatherHistory(Model):
    id = fields.IntField(pk=True)
    time = fields.DatetimeField(null=True)
    city = fields.CharField(max_length=128)
    country_code = fields.CharField(max_length=8)
    count_days = fields.IntField(null=True)
    result = fields.JSONField()

    class Meta:
        ordering = ['-id']
