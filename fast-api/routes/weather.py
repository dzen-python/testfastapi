import datetime
import json
import os
import time

import requests
from fastapi import APIRouter
from geopy.geocoders import Nominatim

from models.weather import WeatherHistory

router = APIRouter()
gn = Nominatim(user_agent='Fast')

date_format_str = '%Y-%m-%dT%H:%M:%S'


@router.get('/', response_model=dict)
async def get_weather(country_code='RU', city='Moscow', date='2021-12-21T23:59:59'):
    result_lo = gn.geocode(city)
    wh = await WeatherHistory.filter(
        time=datetime.datetime.strptime(date, date_format_str).isoformat() + '.000000 +00:00',
        city=city,
        country_code=country_code,
    ).first()
    if wh:
        return wh.result
    params = {
        'lat': result_lo.latitude,
        'lon': result_lo.longitude,
        'appid': os.environ.get('WEATHER_API', '69603c09d96507d176f8c76f0e51abdd'),
        'lang': country_code,
    }
    if datetime.datetime.strptime(date, date_format_str) > datetime.datetime.now():
        response = requests.get(
            'https://api.openweathermap.org/data/2.5/forecast', params=params | {'q': city}
        )
        data = json.loads(response.content)['list']
    else:
        response = requests.get(
            'https://api.openweathermap.org/data/2.5/onecall/timemachine',
            params=params
            | {
                'dt': int(
                    time.mktime(datetime.datetime.strptime(date, date_format_str).timetuple())
                ),
            },
        )
        data = json.loads(response.content)['hourly']
    for i in data:
        if i['dt'] == int(
            time.mktime(datetime.datetime.strptime(date, date_format_str).timetuple())
        ):
            weather = await WeatherHistory.create(
                city=city,
                result=i,
                country_code=country_code,
                time=datetime.datetime.strptime(date, date_format_str),
            )
            return weather.result
