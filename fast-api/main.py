import os

from fastapi import FastAPI
from tortoise.contrib.fastapi import register_tortoise

from routes import weather

app = FastAPI()

app.include_router(router=weather.router)

DATABASE_URL = 'postgres://{}:{}@{}:{}/{}'.format(
    os.environ.get('POSTGRES_USER', 'postgres'),
    os.environ.get('POSTGRES_PASSWORD', '12345678'),
    os.environ.get('POSTGRES_HOST', 'localhost'),
    os.environ.get('POSTGRES_PORT', '5432'),
    os.environ.get('DB_NAME', 'postgres'),
)
# DATABASE_URL = 'postgres://postgres:12345678@localhost:5432/postgres'

# t = {'connections': {
#     'default': {
#         'engine': 'tortoise.backends.asyncpg',
#         'credentials': {
#             'host': 'localhost',
#             'port': '5432',
#             'user': 'tortoise',
#             'password': 'qwerty123',
#             'database': 'test',
#         }
#     }, }
# }

register_tortoise(
    app,
    db_url=DATABASE_URL,
    modules={'models': ['models.weather']},
    generate_schemas=True,
    add_exception_handlers=True,

)
